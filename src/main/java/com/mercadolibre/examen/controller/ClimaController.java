package com.mercadolibre.examen.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mercadolibre.examen.dto.ClimaDto;
import com.mercadolibre.examen.model.Clima;
import com.mercadolibre.examen.model.TipoDeClima;
import com.mercadolibre.examen.repository.ClimaRepository;
import com.mercadolibre.examen.service.ClimaService;

@Controller
@RequestMapping("/clima")
public class ClimaController {
	@Autowired
	private ClimaService climaService;
	
	@Autowired
	private ClimaRepository climaRepository;

    @RequestMapping(value = "/{dia}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClimaDto> getClima(@PathVariable("dia") String dia) {
    	try {
    		Double perimetroMaximo = climaRepository.getPerimetroMaximo();
    		Clima clima = climaRepository.findByDia(Integer.valueOf(dia));
    		ClimaDto climaDto = new ClimaDto();
    		climaDto.setDia(dia);
    		
    		if (TipoDeClima.sequia.equals(clima.getClima())) {
    			climaDto.setClima("Sequia");
    		} else if (TipoDeClima.copt.equals(clima.getClima())) {
    			climaDto.setClima("Condiciones optimas de presion y temperatura");
    		} else if (TipoDeClima.lluvia.equals(clima.getClima())) {
    			if (clima.getPerimetro().compareTo(perimetroMaximo) == 0) {
    				climaDto.setClima("Lluvia de maxima intensidad");
        		} else {
        			climaDto.setClima("Lluvia");
        		}
    		} else {
    			climaDto.setClima("Normal");
    		}
    		return new ResponseEntity<ClimaDto>(climaDto, HttpStatus.OK);
    	} catch (Exception e) {
    		ClimaDto climaDto = new ClimaDto();
    		climaDto.setDia("Invalido, ingrese un numero entre 0 y 3649");
    		climaDto.setClima("");
    		return new ResponseEntity<ClimaDto>(climaDto, HttpStatus.OK);
    	}
    }
    
    @RequestMapping(value = "/init", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> init() {
    	climaService.init();
        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }
	
}
