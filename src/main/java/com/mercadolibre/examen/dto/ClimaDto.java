package com.mercadolibre.examen.dto;

public class ClimaDto {
	private String dia;
	private String clima;
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getClima() {
		return clima;
	}
	public void setClima(String clima) {
		this.clima = clima;
	}
}
