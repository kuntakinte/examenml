package com.mercadolibre.examen.dto;

import com.mercadolibre.examen.model.Planeta;

public class SegmentoDto {
	private static final double error = 0.001d;
	
	// el punto 1
	private Double x1;
	private Double y1;
	// el punto 2
	private Double x2;
	private Double y2;
	
	private Double pendiente;
	private Double ordenada;
	
	public SegmentoDto(Planeta planeta1, Planeta planeta2) {
		this.x1 = planeta1.getDistanciaAlSol() * Math.cos(Math.toRadians(planeta1.getAngulo()));
		this.x2 = planeta2.getDistanciaAlSol() * Math.cos(Math.toRadians(planeta2.getAngulo()));
		this.y1 = planeta1.getDistanciaAlSol() * Math.sin(Math.toRadians(planeta1.getAngulo()));
		this.y2 = planeta2.getDistanciaAlSol() * Math.sin(Math.toRadians(planeta2.getAngulo()));
		this.pendiente = (y2 - y1) / (x2 - x1);
		this.ordenada = y1 - this.pendiente * x1;
	}
	
	public Double getPendiente() {
		return pendiente;
	}
	public Double getOrdenada() {
		return ordenada;
	}
	public Boolean pertenceASegmento(Planeta planeta) {
		double a = planeta.getDistanciaAlSol() * Math.sin(Math.toRadians(planeta.getAngulo()));
		double b = this.pendiente * planeta.getDistanciaAlSol() * Math.cos(Math.toRadians(planeta.getAngulo())) + this.getOrdenada();
		return (Math.abs(a - b) < error);
	}
	
	public Boolean segmentoIntersectaEjeXPositivo() {
		// evitamos dividir por 0
		if (this.pendiente == 0 && ordenada != 0) return false;
		if (this.pendiente == 0 && ordenada == 0) return true;
		
		Double x = - this.ordenada / this.pendiente;
		Double xMayor = x1 > x2 ? x1 : x2;
		Double xMenor = x1 < x2 ? x1 : x2;
		if (x > 0 && x < xMayor && x > xMenor) {
			return true;
		}
		
		return false;
	}
	
	public Boolean segmentoIntersectaEjeXNegativo() {
		// evitamos dividir por 0
		if (this.pendiente == 0 && ordenada != 0) return false;
		if (this.pendiente == 0 && ordenada == 0) return true;
		
		Double x = - this.ordenada / this.pendiente;
		Double xMayor = x1 > x2 ? x1 : x2;
		Double xMenor = x1 < x2 ? x1 : x2;
		if (x < 0 && x < xMayor && x > xMenor) {
			return true;
		}
		
		return false;
	}
	
	public Boolean segmentoIntersectaEjeYPositivo() {
		Double y = this.ordenada;
		Double yMayor = y1 > y2 ? y1 : y2;
		Double yMenor = y1 < y2 ? y1 : y2;
		if (y > 0 && y < yMayor && y > yMenor) {
			return true;
		}
		
		return false;
	}
	
	public Boolean segmentoIntersectaEjeYNegativo() {
		Double y = this.ordenada;
		Double yMayor = y1 > y2 ? y1 : y2;
		Double yMenor = y1 < y2 ? y1 : y2;
		if (y < 0 && y < yMayor && y > yMenor) {
			return true;
		}
		return false;
	}
	
	public Double getPerimetro() {
		return Math.sqrt(Math.pow(y2-y1, 2)+Math.pow(x2-x1, 2));
	}
}
