package com.mercadolibre.examen.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "Clima", uniqueConstraints = {@UniqueConstraint(columnNames = {"dia"})})
public class Clima {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
	private Integer dia;
	@Enumerated(EnumType.STRING)
	private TipoDeClima clima;
	private Double perimetro;
	
	public Clima() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDia() {
		return dia;
	}

	public void setDia(Integer dia) {
		this.dia = dia;
	}

	public TipoDeClima getClima() {
		return clima;
	}

	public void setClima(TipoDeClima clima) {
		this.clima = clima;
	}

	public Double getPerimetro() {
		return perimetro;
	}

	public void setPerimetro(Double perimetro) {
		this.perimetro = perimetro;
	}
	
}
