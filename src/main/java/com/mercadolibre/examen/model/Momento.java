package com.mercadolibre.examen.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "Momento", uniqueConstraints = {@UniqueConstraint(columnNames = {"dia"})})
public class Momento {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
	private String dia;
//	private List<Planeta> planetas;
	private TipoDeClima clima;
	private Float perimetro;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
//	public List<Planeta> getPlanetas() {
//		return planetas;
//	}
//	public void setPlanetas(List<Planeta> planetas) {
//		this.planetas = planetas;
//	}
	public TipoDeClima getClima() {
		return clima;
	}
	public void setClima(TipoDeClima clima) {
		this.clima = clima;
	}
	public Float getPerimetro() {
		return perimetro;
	}
	public void setPerimetro(Float perimetro) {
		this.perimetro = perimetro;
	}
	
	
}
