package com.mercadolibre.examen.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "Planeta", uniqueConstraints = {@UniqueConstraint(columnNames = {"nombre"})})
public class Planeta {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
	private String nombre;
	private Integer distanciaAlSol;
	private Integer velocidadAngular;
	private Integer angulo;
	
	public Planeta() {
	}
	
	/**
	 * Usado para facilitar la creacion de test automatizados
	 */
	public Planeta(Integer distanciaAlSol, Integer angulo) {
		this.distanciaAlSol = distanciaAlSol;
		this.angulo = angulo;
	}
	
	/**
	 * Usado para facilitar la creacion de test automatizados
	 */
	public Planeta(Integer angulo) {
		this.angulo = angulo;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getDistanciaAlSol() {
		return distanciaAlSol;
	}
	public void setDistanciaAlSol(Integer distanciaAlSol) {
		this.distanciaAlSol = distanciaAlSol;
	}
	public Integer getVelocidadAngular() {
		return velocidadAngular;
	}
	public void setVelocidadAngular(Integer velocidadAngular) {
		this.velocidadAngular = velocidadAngular;
	}
	public Integer getAngulo() {
		return angulo;
	}
	public void setAngulo(Integer angulo) {
		this.angulo = angulo;
	}
	public void mover() {
		this.angulo = this.angulo + this.velocidadAngular;
		if (this.velocidadAngular > 0) {
			if (this.angulo >= 360) {
				this.angulo = this.angulo - 360;
			}
		}
		if (this.velocidadAngular < 0) {
			if (this.angulo < 0) {
				this.angulo = this.angulo + 360;
			}
		}
	}
}
