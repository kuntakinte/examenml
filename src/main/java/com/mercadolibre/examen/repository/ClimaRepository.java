package com.mercadolibre.examen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mercadolibre.examen.model.Clima;

@Repository
public interface ClimaRepository extends JpaRepository<Clima, Long> {
	public Clima findByDia(Integer dia);
	@Query(value = "SELECT max(perimetro) FROM Clima WHERE clima = 'lluvia'", nativeQuery = true)
	public Double getPerimetroMaximo();
}
