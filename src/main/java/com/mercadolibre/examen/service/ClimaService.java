package com.mercadolibre.examen.service;

import com.mercadolibre.examen.model.Planeta;
import com.mercadolibre.examen.model.TipoDeClima;

public interface ClimaService {
	public void init();
	public TipoDeClima getClima(Planeta ferengi, Planeta vulcano, Planeta betasoide);
}
