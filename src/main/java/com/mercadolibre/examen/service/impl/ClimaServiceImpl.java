package com.mercadolibre.examen.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercadolibre.examen.dto.SegmentoDto;
import com.mercadolibre.examen.model.Clima;
import com.mercadolibre.examen.model.Planeta;
import com.mercadolibre.examen.model.TipoDeClima;
import com.mercadolibre.examen.repository.ClimaRepository;
import com.mercadolibre.examen.service.ClimaService;

@Service
public class ClimaServiceImpl implements ClimaService {
	private static final double maxError = 0.1d;
	
	@Autowired
	private ClimaRepository climaRepository;
	
	public void init() {
		climaRepository.deleteAll();
		
		Planeta ferengi = new Planeta();
		ferengi.setDistanciaAlSol(500);
		ferengi.setAngulo(90);
		ferengi.setVelocidadAngular(-1);
		Planeta vulcano= new Planeta();
		vulcano.setDistanciaAlSol(1000);
		vulcano.setAngulo(90);
		vulcano.setVelocidadAngular(5);
		Planeta betasoide = new Planeta();
		betasoide.setDistanciaAlSol(2000);
		betasoide.setAngulo(90);
		betasoide.setVelocidadAngular(-3);
		
		List<Clima> climas = new ArrayList<Clima>();
		for (int i = 0; i < (365*10) ; i++) {
			Clima clima = new Clima();
			clima.setDia(i);
			clima.setClima(this.getClima(ferengi, vulcano, betasoide));
			clima.setPerimetro(this.getPerimetro(ferengi, vulcano, betasoide));
			climas.add(clima);
			ferengi.mover();
			vulcano.mover();
			betasoide.mover();
		}
		climaRepository.saveAll(climas);
	}

	@Override
	public TipoDeClima getClima(Planeta ferengi, Planeta vulcano, Planeta betasoide) {
		
		// si los angulos son iguales o dos son iguales y uno es 180 grados menos o mas que los otros dos entonces hay sequia
		// paso todos los angulos que esten en el tercer y cuarto cuadrante el primero y segundo
		Integer tmpAngulo1 = ferengi.getAngulo() < 180 ? ferengi.getAngulo() : ferengi.getAngulo() - 180;
		Integer tmpAngulo2 = vulcano.getAngulo() < 180 ? vulcano.getAngulo() : vulcano.getAngulo() - 180;
		Integer tmpAngulo3 = betasoide.getAngulo() < 180 ? betasoide.getAngulo() : betasoide.getAngulo() - 180;
		if (tmpAngulo1.equals(tmpAngulo2) && tmpAngulo2.equals(tmpAngulo3)) {
			return TipoDeClima.sequia;
		}

		SegmentoDto segmentoFV = new SegmentoDto(ferengi, vulcano);
		SegmentoDto segmentoVB = new SegmentoDto(vulcano, betasoide);
		if (Math.abs(segmentoFV.getPendiente() - segmentoVB.getPendiente()) < maxError) {
			return TipoDeClima.copt;
		}
		
		SegmentoDto segmentoBF = new SegmentoDto(betasoide, ferengi);
		if ( (segmentoFV.segmentoIntersectaEjeXNegativo() || segmentoVB.segmentoIntersectaEjeXNegativo() || segmentoBF.segmentoIntersectaEjeXNegativo()) && 
				(segmentoFV.segmentoIntersectaEjeXPositivo() || segmentoVB.segmentoIntersectaEjeXPositivo() || segmentoBF.segmentoIntersectaEjeXPositivo()) && 
				(segmentoFV.segmentoIntersectaEjeYNegativo() || segmentoVB.segmentoIntersectaEjeYNegativo() || segmentoBF.segmentoIntersectaEjeYNegativo()) &&
				(segmentoFV.segmentoIntersectaEjeYPositivo() || segmentoVB.segmentoIntersectaEjeYPositivo() || segmentoBF.segmentoIntersectaEjeYPositivo())) {
			return TipoDeClima.lluvia;
		}

		return TipoDeClima.ninguno;
	}
	
	public Double getPerimetro(Planeta ferengi, Planeta vulcano, Planeta betasoide) {
		SegmentoDto segmentoFV = new SegmentoDto(ferengi, vulcano);
		SegmentoDto segmentoVB = new SegmentoDto(vulcano, betasoide);
		SegmentoDto segmentoBF = new SegmentoDto(betasoide, ferengi);
		return segmentoFV.getPerimetro() + segmentoVB.getPerimetro() + segmentoBF.getPerimetro();
	}
}
