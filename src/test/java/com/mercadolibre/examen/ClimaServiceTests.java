package com.mercadolibre.examen;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.mercadolibre.examen.model.Planeta;
import com.mercadolibre.examen.model.TipoDeClima;
import com.mercadolibre.examen.service.ClimaService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ClimaServiceTests {
	@Autowired
	private ClimaService climaService;

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void sequiaTest() {
	
		assertTrue(TipoDeClima.sequia.equals(climaService.getClima(new Planeta(0), new Planeta(0), new Planeta(0))));
		assertTrue(TipoDeClima.sequia.equals(climaService.getClima(new Planeta(45), new Planeta(45), new Planeta(45))));
		assertTrue(TipoDeClima.sequia.equals(climaService.getClima(new Planeta(90), new Planeta(90), new Planeta(90))));
		assertTrue(TipoDeClima.sequia.equals(climaService.getClima(new Planeta(135), new Planeta(135), new Planeta(135))));
		assertTrue(TipoDeClima.sequia.equals(climaService.getClima(new Planeta(180), new Planeta(180), new Planeta(180))));
		assertTrue(TipoDeClima.sequia.equals(climaService.getClima(new Planeta(225), new Planeta(225), new Planeta(225))));
		assertTrue(TipoDeClima.sequia.equals(climaService.getClima(new Planeta(270), new Planeta(270), new Planeta(270))));
		assertTrue(TipoDeClima.sequia.equals(climaService.getClima(new Planeta(315), new Planeta(315), new Planeta(315))));
		
		assertTrue(TipoDeClima.sequia.equals(climaService.getClima(new Planeta(0), new Planeta(180), new Planeta(0))));
		assertTrue(TipoDeClima.sequia.equals(climaService.getClima(new Planeta(45), new Planeta(225), new Planeta(225))));
		assertTrue(TipoDeClima.sequia.equals(climaService.getClima(new Planeta(270), new Planeta(90), new Planeta(90))));
		assertTrue(TipoDeClima.sequia.equals(climaService.getClima(new Planeta(190), new Planeta(190), new Planeta(10))));
		
//		assertFalse(TipoDeClima.sequia.equals(climaService.getClima(new Planeta(190), new Planeta(190), new Planeta(11))));
	}
	
	@Test
	public void lluviaTest() {
		assertTrue(TipoDeClima.lluvia.equals(climaService.getClima(new Planeta(500,80), new Planeta(1000,304), new Planeta(2000,164))));
	}
	
	@Test
	public void coptTest() {
		assertTrue(TipoDeClima.copt.equals(climaService.getClima(new Planeta(500,123), new Planeta(1000,344), new Planeta(2000,338))));
	}

}
